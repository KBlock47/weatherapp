# Weather App / Yearbook App

Clean the project when you import it if you're going to straight up use it. I would definitely just look at how the SimpleREST class is implemented along with how the json string (data fetched back from the weather API) is also processed. That is the hardest part. Then there is the part where you have to actually show the data. I used a RecycleView because I never used one before but it seems a little bit more complicated than a regular list view

You need to add the Internet permission in your app manifest and this line in your dependencies file which is located under buildgradle (Module: app) implementation 'com.android.volley:volley:1.0.0'

