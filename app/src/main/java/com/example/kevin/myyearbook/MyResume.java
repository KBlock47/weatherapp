package com.example.kevin.myyearbook;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class MyResume extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_resume);
        WebView view = findViewById(R.id.webview);
        view.loadUrl("https://www.sjcny.edu/");
    }
}
