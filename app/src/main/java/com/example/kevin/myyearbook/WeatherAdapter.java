package com.example.kevin.myyearbook;

import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Kevin on 2/27/2018.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {

    private List<ArrayMap> dayData;

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView day;
        public TextView desc;
        public TextView deg;
        public ImageView img;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            day = v.findViewById(R.id.day);
            desc = v.findViewById(R.id.desc);
            deg = v.findViewById(R.id.deg);
            img = v.findViewById(R.id.img);
        }
    }

    public WeatherAdapter(List<ArrayMap> map){
        dayData = map;
    }

    @Override
    public WeatherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View v = inflater.inflate(R.layout.weather_row,parent,false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(WeatherAdapter.ViewHolder holder, int i) {
        holder.desc.setText((String)dayData.get(i).get("desc"));
        holder.day.setText((String)dayData.get(i).get("day"));
        holder.deg.setText(String.valueOf(dayData.get(i).get("temp"))+"°F");
        holder.img.setImageResource((int)dayData.get(i).get("img"));
    }

    public void DeleteData(){
        if (!dayData.isEmpty()){
           dayData.clear();
        }
    }

    @Override
    public int getItemCount() {
        return dayData.size();
    }
}
