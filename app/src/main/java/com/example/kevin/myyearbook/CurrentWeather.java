package com.example.kevin.myyearbook;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.ArrayMap;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CurrentWeather extends AppCompatActivity {

    private static long lastFetch = 0;
    private static String apiKey = "cbcab9e9fdd7c8a6cea406a70d31f0db";
    private static String apiUrl = "http://api.openweathermap.org/data/2.5/forecast?zip=";
    private List<ArrayMap> dayData = new ArrayList<>();

    private RecyclerView recycleView;
    private RecyclerView.Adapter recycleAdapter;

    private int PERMISSION_TO_SNOOP = 1;

    public void HideKeyboard() {
        try{
            InputMethodManager keyboard = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
            keyboard.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }catch(NullPointerException e){

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_weather);
        recycleView = findViewById(R.id.listviewer);
        recycleView.setLayoutManager(new LinearLayoutManager(this));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_TO_SNOOP);
            }
        }else{
            //We already have perms
            FetchWeather(true);
        }
    }

    public void PopulateDay(int day, ArrayMap data) {
        dayData.add(day - 1, data);
        if (day == 5) {
            FinishDayBuffer();
        }
    }

    public void FinishDayBuffer() {
        recycleAdapter = new WeatherAdapter(dayData);
        recycleView.setAdapter(recycleAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        if (requestCode == PERMISSION_TO_SNOOP){
            FetchWeather(true);
        }else{
            Toast.makeText(this, "I cannot snoop therefor u may not see the weather", Toast.LENGTH_SHORT).show();
        }
    }

    private Location getLastKnownLocation(LocationManager manager) {
        //Find all providers we used a GPS with (network or gps)
        List<String> providers = manager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location loc = manager.getLastKnownLocation(provider);
            if (loc == null) {
                continue;
            }
            if (bestLocation == null || loc.getAccuracy() < bestLocation.getAccuracy()){
                bestLocation = loc;
            }
        }
        return bestLocation;
    }

    public void FetchWeatherWrapper(View v){
        FetchWeather(false);
    }

    public void FetchWeather(boolean checkLoc) {
        if (lastFetch > System.currentTimeMillis()) {
            return;
        }

        if (checkLoc) {
            LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            Location loc = null;

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_TO_SNOOP);
                }
            }else{
                loc = getLastKnownLocation(locationManager);
            }

            if (loc == null){
                Toast.makeText(this,"Location was invalid", Toast.LENGTH_SHORT).show();
                return;
            }

            double lat = loc.getLatitude();
            double longe = loc.getLongitude();

            Geocoder geo = new Geocoder(this, Locale.ENGLISH);
            List<Address> addr;
            try {
                addr = geo.getFromLocation(lat,longe,1);
            } catch (IOException e) {
                Toast.makeText(this,"Uh-oh no zip", Toast.LENGTH_SHORT).show();
                return;
            }

            EditText zipObj = findViewById(R.id.zipcode);
            zipObj.setText(addr.get(0).getPostalCode());
        }



         lastFetch = System.currentTimeMillis() + 1000;
         EditText zipObj = findViewById(R.id.zipcode);
         String zipcode = zipObj.getText().toString();
         final Context context = this;
         final ProgressBar spinner = findViewById(R.id.progress);
         final TextView cityLoc = findViewById(R.id.forcastTxt);

         if (zipcode.length() < 5){
             Toast.makeText(this,"Your zipcode is invalid!",Toast.LENGTH_LONG).show();
             return;
         }

        HideKeyboard();
        SimpleREST getData = new SimpleREST(this);

        spinner.setVisibility(View.VISIBLE);
        recycleView.setVisibility(View.INVISIBLE);
        cityLoc.setVisibility(View.INVISIBLE);

        if (recycleAdapter != null && recycleAdapter instanceof WeatherAdapter){
            ((WeatherAdapter)recycleAdapter).DeleteData();
        }

        try {
            getData.GetJSON(apiUrl + zipcode + "&appid=" + apiKey, new VolleyCallback() {
                @Override
                public void onSuccessResponse(String result) throws JSONException {
                    spinner.setVisibility(View.INVISIBLE);
                    recycleView.setVisibility(View.VISIBLE);

                    JSONObject json = new JSONObject(result);
                    JSONObject cityData = (JSONObject) json.get("city");
                    JSONArray listData = json.getJSONArray("list");

                    cityLoc.setVisibility(View.VISIBLE);
                    String cityName = cityData.getString("name");
                    cityLoc.setText("Showing Data For "+cityName);

                    int day = 1;

                    for(int v = 0; v < listData.length(); v++){
                        JSONObject content = listData.getJSONObject(v);
                        boolean isGood = false;
                        if (content.has("dt_txt")){
                            isGood = Weather.UseData(content.getString("dt_txt"));
                        }else{
                            continue;
                        }

                        if (!isGood){ continue; }

                        String temp = Weather.GetTemp(content.getJSONObject("main"));
                        String weatherDesc = Weather.GetDesc(content.getJSONArray("weather"));

                        ArrayMap data = new ArrayMap();

                        data.put("day",Weather.ParseDay(content.getString("dt_txt")));
                        data.put("temp",Weather.KelvinToFaren(temp));
                        data.put("img",Weather.GetImg(weatherDesc));
                        data.put("desc",weatherDesc);

                        PopulateDay(day,data);

                        if (day < 5) {
                            day++;
                        }
                       //Log.d("DATA", "onSuccessResponse: ");
                    }
                }

                @Override
                public void onFailure(String err) {
                    spinner.setVisibility(View.INVISIBLE);
                    Toast.makeText(context,"City data not found",Toast.LENGTH_LONG).show();
                }

            });
        } catch (IOException e) {
            spinner.setVisibility(View.INVISIBLE);
            e.printStackTrace();
        }
    }
}
