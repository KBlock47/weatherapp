package com.example.kevin.myyearbook;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Kevin on 2/27/2018.
 */

public class Weather {
    public static int KelvinToFaren(String data){
        //Log.d("Parsing",data);
        double parse = 0.0;

        try{
            parse = Double.valueOf(data);
        }catch(NullPointerException | NumberFormatException e){
            Log.d("Error parsing",e.getMessage());
            parse = 277.53;
        }

        double wat = parse * 1.8;


        Double result = (parse * 1.8)-459.67;
        return result.intValue();
    }

    public static boolean UseData(String date){
        return date.contains("12:00:00");
    }

    private static String GetDay(int day){
        if (day == Calendar.MONDAY){
            return "Monday";
        }else if(day == Calendar.TUESDAY){
            return "Tuesday";
        }else if(day == Calendar.WEDNESDAY){
            return "Wednesday";
        }else if(day == Calendar.THURSDAY){
            return "Thursday";
        }else if(day == Calendar.FRIDAY){
            return "Friday";
        }else if(day == Calendar.SATURDAY){
            return "Saturday";
        }else if(day == Calendar.SUNDAY) {
            return "Sunday";
        }
        return "Unkown";
    }

    public static String ParseDay(String data){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try{
            Date d = dateFormat.parse(data);
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            return GetDay(c.get(Calendar.DAY_OF_WEEK));
        } catch (ParseException e) {
            e.printStackTrace();
            return GetDay(-1);
        }
    }

    public static String GetTemp(JSONObject json) throws JSONException {
        return json.getString("temp");
    }

    public static String GetDesc(JSONArray json) throws JSONException {
        for(int i = 0; i < json.length(); i++){
            JSONObject obj = json.getJSONObject(i);
            if (obj.has("main")){
                Log.d("Type",obj.getString("description"));
                return obj.getString("description").toUpperCase();
            }
        }
        return "ERROR";
    }

    public static int GetImg(String info){
        String lower = info.toLowerCase();
        int clouds = lower.indexOf("clouds");
        int sunny = lower.indexOf("clear");
        int rain = lower.indexOf("rain");
        int snow = lower.indexOf("snow");
        int storm = lower.indexOf("thunder");
        int storm2 = lower.indexOf("storm");

        if (clouds != -1){
            return R.drawable.clouds;
        }else if(sunny != -1){
            return R.drawable.sun;
        }else if(rain != -1){
            return R.drawable.rain;
        }else if(snow != -1){
            return R.drawable.snow;
        }else if(storm != -1){
            return R.drawable.thunder;
        }else if(storm2 != -1){
            return R.drawable.thunder;
        }else{
            return R.drawable.wind;
        }
    }
}
