package com.example.kevin.myyearbook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void GotoWeather(View v){
        Intent intent = new Intent(this,CurrentWeather.class);
        startActivity(intent);
    }

    public void GotoResume(View v){
        Intent intent = new Intent(this,MyResume.class);
        startActivity(intent);
    }
}
