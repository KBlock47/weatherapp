package com.example.kevin.myyearbook;

import org.json.JSONException;

/**
 * Created by Kevin on 2/27/2018.
 */

public interface VolleyCallback {
    void onSuccessResponse(String result) throws JSONException;
    void onFailure(String err);
}
